package com.nwpu.TimeMaster.adapter;

import android.content.Intent;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEntity;
import com.nwpu.TimeMaster.activity.EditToDoActivity;
import com.nwpu.TimeMaster.data.ToDo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static androidx.core.content.ContextCompat.startActivity;

/**
 *日历视图中待办事项显示
 */
public class WeekViewAdapter extends WeekView.SimpleAdapter<ToDo> {

    /**
     *设置待办事项显示样式
     *
     * @param item 当前登录用户的待办事项
     * @return weekViewEntity实体类
     */
    @Override
    public WeekViewEntity onCreateEntity(ToDo item) {
        Calendar calStart = Calendar.getInstance();
        calStart.setTime(item.getStartDate());
        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(item.getEndDate());
        int[] colors = {0xFFF45E5E, 0xFFFFD500, 0xFF288F81, 0xFF070B59, 0xFF8480ED};

        WeekViewEntity.Style style = new WeekViewEntity.Style.Builder().setBackgroundColor(colors[item.getTag()]).build();
        return new WeekViewEntity.Event.Builder<>(item)
                .setId(item.getId())
                .setTitle(item.getTitle())
                .setStartTime(calStart)
                .setEndTime(calEnd)
                .setStyle(style)
                .build();
    }

    /**
     * 待办事项点击事项，跳转到编辑页面
     *
     * @param data 点击的待办事项
     */
    @Override
    public void onEventClick(ToDo data) {
        Intent intent = new Intent(getContext(), EditToDoActivity.class);
        intent.putExtra("id", data.getId());
        startActivity(getContext(), intent, null);
        super.onEventClick(data);
    }
}
