package com.nwpu.TimeMaster.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.data.ToDo;
import com.nwpu.TimeMaster.service.SyncService;
import org.litepal.LitePal;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * 修改待办事项
 */
public class EditToDoActivity extends AppCompatActivity {
    /**
     * 待办事项
     */
    private ToDo todo;
    /**
     * 优先级
     */
    private int priority;
    /**
     * 标签
     */
    private int tag;
    /**
     * 记录是否提醒
     */
    private boolean[] remindItems;
    /**
     * 日期选择对话框
     */
    private DatePickerDialog startDatePicker, endDatePicker;
    /**
     * 时间选择对话框
     */
    private TimePickerDialog startTimePicker, endTimePicker;
    /**
     * 日记文字显示
     */
    private TextView startDateText, endDateText, startTimeText, endTimeText;
    /**
     * 记录选择的日期
     */
    private Calendar startDate, endDate;

    /**
     * 活动启动时调用
     *
     * @param savedInstanceState 保存的实例状态
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_to_do);

        setTitle("编辑待办事项");
        int todoId = getIntent().getIntExtra("id", 0);
        todo = LitePal.find(ToDo.class, todoId);

        EditText oldTitle = findViewById(R.id.title);
        oldTitle.setText(todo.getTitle());

        Calendar calendarstart = Calendar.getInstance();
        calendarstart.setTime(todo.getStartDate());
        startDate = calendarstart;
        Calendar calendarend = Calendar.getInstance();
        calendarend.setTime(todo.getEndDate());
        endDate = calendarend;

        startDateText = findViewById(R.id.start_date);
        startDateText.setText(format(startDate, 0));

        endDateText = findViewById(R.id.end_date);
        endDateText.setText(format(endDate, 0));

        startTimeText = findViewById(R.id.start_time);
        startTimeText.setText(format(startDate, 1));

        endTimeText = findViewById(R.id.end_time);
        endTimeText.setText(format(endDate, 1));

        EditText oldDetail = findViewById(R.id.detail);
        oldDetail.setText(todo.getDetail());

        TextView priority = findViewById(R.id.priority);
        String[] priorities = getResources().getStringArray(R.array.priority);
        priority.setText(priorities[todo.getPriority()]);
        this.priority = todo.getPriority();
        priority.setOnClickListener(v -> {
            AlertDialog.Builder priorityBuilder = new AlertDialog.Builder(this);
            priorityBuilder.setTitle("选择优先级")
                    .setItems(priorities, (dialog, which) -> {
                        this.priority = which;
                        priority.setText(priorities[which]);
                    })
                    .create().show();
        });

        TextView tag = findViewById(R.id.tag);
        String[] tags = getResources().getStringArray(R.array.tag);
        tag.setText(tags[todo.getTag()]);
        this.tag = todo.getTag();
        tag.setOnClickListener(v -> {
            AlertDialog.Builder tagBuilder = new AlertDialog.Builder(this);
            tagBuilder.setTitle("选择标签")
                    .setItems(tags, (dialog, which) -> {
                        this.tag = which;
                        tag.setText(tags[which]);
                    })
                    .create().show();
        });

        ImageButton save = findViewById(R.id.save);
        save.setOnClickListener(v -> {
            if (startDate.after(endDate)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("开始时间不得晚于结束时间。")
                        .setPositiveButton("确定", null)
                        .create().show();
            } else {
                save.setClickable(false);
                todo.setTitle(oldTitle.getText().toString());
                todo.setDetail(oldDetail.getText().toString());
                todo.setStartDate(startDate.getTime());
                todo.setEndDate(endDate.getTime());
                todo.setPriority(this.priority);
                todo.setTag(this.tag);
                todo.setStatus(1);
                todo.update(todo.getId());

                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        ImageView delete = findViewById(R.id.delete);
        delete.setOnClickListener(V -> {
            delete.setClickable(false);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("确定删除吗？")
                    .setPositiveButton("确定", (dialog, which) -> {
                        todo.setStatus(-1);
                        todo.update(todo.getId());
                        Intent intent = new Intent(getApplication(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }).setNegativeButton("取消", null)
                    .create().show();
        });

        TextView reminder = findViewById(R.id.reminder);
        reminder.setOnClickListener(v -> {
            AlertDialog.Builder tagBuilder = new AlertDialog.Builder(this);
            String[] reminders = getResources().getStringArray(R.array.reminder);
            if (remindItems == null) {
                remindItems = new boolean[reminders.length];
            }

            tagBuilder.setTitle("选择提醒")
                    .setMultiChoiceItems(reminders, remindItems, (dialog, which, isChecked) -> remindItems[which] = isChecked)
                    .setPositiveButton("确定", null)
                    .setNegativeButton("取消", (dialog, which) -> remindItems = new boolean[reminders.length])
                    .create().show();
        });

        startDatePicker = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
            startDate.set(year, month, dayOfMonth);
            startDateText.setText(format(startDate, 0));
            changeColor();
        }, startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));

        endDatePicker = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
            endDate.set(year, month, dayOfMonth);
            endDateText.setText(format(endDate, 0));
            changeColor();
        }, endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));

        startTimePicker = new TimePickerDialog(this, (view, hourOfDay, minute) -> {
            startDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
            startDate.set(Calendar.MINUTE, minute);
            startTimeText.setText(format(startDate, 1));
            changeColor();
        }, startDate.get(Calendar.HOUR_OF_DAY), startDate.get(Calendar.MINUTE), true);

        endTimePicker = new TimePickerDialog(this, (view, hourOfDay, minute) -> {
            endDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
            endDate.set(Calendar.MINUTE, minute);
            endTimeText.setText(format(endDate, 1));
            changeColor();
        }, endDate.get(Calendar.HOUR_OF_DAY), endDate.get(Calendar.MINUTE), true);

        startDateText.setOnClickListener(v -> startDatePicker.show());
        endDateText.setOnClickListener(v -> endDatePicker.show());
        startTimeText.setOnClickListener(v -> startTimePicker.show());
        endTimeText.setOnClickListener(v -> endTimePicker.show());
    }

    /**
     * 日期格式化
     *
     * @param calendar 日期
     * @param m        模式
     * @return 格式化的日期
     */
    public static String format(Calendar calendar, int m) {
        SimpleDateFormat sdf;
        if (m == 0) {
            sdf = new SimpleDateFormat("yyyy年MM月dd日 E");
            return sdf.format(calendar.getTime());
        } else if (m == 1) {
            sdf = new SimpleDateFormat("HH:mm");
            return sdf.format(calendar.getTime());
        } else { return "";}
    }

    /**
     * 日期错误时显示红色
     */
    private void changeColor() {
        if (startDate.after(endDate)) {
            startDateText.setTextColor(Color.RED);
            startTimeText.setTextColor(Color.RED);
        } else {
            startDateText.setTextColor(Color.BLACK);
            startTimeText.setTextColor(Color.BLACK);
        }
    }

    /**
     * 在活动停止时调用
     */
    @Override
    protected void onStop() {
        startService(new Intent(this, SyncService.class));
        super.onStop();
    }
}