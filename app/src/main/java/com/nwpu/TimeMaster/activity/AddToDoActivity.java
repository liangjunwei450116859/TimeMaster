package com.nwpu.TimeMaster.activity;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.data.ToDo;
import com.nwpu.TimeMaster.data.User;
import com.nwpu.TimeMaster.service.SyncService;
import com.nwpu.TimeMaster.service.TimeService;
import org.json.JSONException;
import org.json.JSONObject;
import org.litepal.LitePal;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

/**
 * 添加待办事项
 */
public class AddToDoActivity extends AppCompatActivity {
    /**
     * 用户
     */
    private User user;
    /**
     * 优先级
     */
    private int priority = 3;
    /**
     * 标签
     */
    private int tag = 4;
    /**
     * 记录是否提醒
     */
    private boolean[] remindItems;
    /**
     * 日期选择对话框
     */
    private DatePickerDialog startDatePicker, endDatePicker;
    /**
     * 时间选择对话框
     */
    private TimePickerDialog startTimePicker, endTimePicker;
    /**
     * 日记文字显示
     */
    private TextView startDateText, endDateText, startTimeText, endTimeText;
    /**
     * 记录选择的日期
     */
    private Calendar startDate, endDate;
    /**
     * 闹钟管理服务
     */
    private AlarmManager alarmManager;

    /**
     * 活动启动时调用
     *
     * @param savedInstanceState 保存的实例状态
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_do);

        setTitle("添加待办事项");
        user = (LitePal.find(User.class, getSharedPreferences("user", Context.MODE_PRIVATE).getInt("user_id", -1)));

        startDate = Calendar.getInstance(Locale.CHINA);
        endDate = Calendar.getInstance(Locale.CHINA);
        endDate.add(Calendar.HOUR_OF_DAY, 1);

        startDateText = findViewById(R.id.start_date);
        startDateText.setText(format(startDate, 0));

        endDateText = findViewById(R.id.end_date);
        endDateText.setText(format(endDate, 0));

        startTimeText = findViewById(R.id.start_time);
        startTimeText.setText(format(startDate, 1));

        endTimeText = findViewById(R.id.end_time);
        endTimeText.setText(format(endDate, 1));

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);


        TextView priority = findViewById(R.id.priority);
        priority.setOnClickListener(v -> {
            AlertDialog.Builder priorityBuilder = new AlertDialog.Builder(this);
            String[] priorities = getResources().getStringArray(R.array.priority);
            priorityBuilder.setTitle("选择优先级")
                    .setItems(priorities, (dialog, which) -> {
                        this.priority = which;
                        priority.setText(priorities[which]);
                    })
                    .create().show();
        });

        TextView tag = findViewById(R.id.tag);
        tag.setOnClickListener(v -> {
            AlertDialog.Builder tagBuilder = new AlertDialog.Builder(this);
            String[] tags = getResources().getStringArray(R.array.tag);
            tagBuilder.setTitle("选择标签")
                    .setItems(tags, (dialog, which) -> {
                        this.tag = which;
                        tag.setText(tags[which]);
                    })
                    .create().show();
        });

        ImageButton save = findViewById(R.id.save);
        save.setOnClickListener(v -> {
            EditText title = findViewById(R.id.title);
            EditText detail = findViewById(R.id.detail);
            if (startDate.after(endDate)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("开始时间不得晚于结束时间")
                        .setPositiveButton("确定", null)
                        .create().show();
            } else {
                save.setClickable(false);
                new ToDo(user, startDate.getTime(), endDate.getTime(), title.getText().toString(), detail.getText().toString(), this.priority, this.tag, 0, new Date(0)).save();
                reminder(title.getText().toString());

                Intent intent = new Intent(getApplication(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        TextView reminder = findViewById(R.id.reminder);
        reminder.setOnClickListener(v -> {
            AlertDialog.Builder tagBuilder = new AlertDialog.Builder(this);
            String[] reminders = getResources().getStringArray(R.array.reminder);
            if (remindItems == null) {
                remindItems = new boolean[reminders.length];
            }

            tagBuilder.setTitle("选择提醒")
                    .setMultiChoiceItems(reminders, remindItems, (dialog, which, isChecked) -> remindItems[which] = isChecked)
                    .setPositiveButton("确定", null)
                    .setNegativeButton("取消", (dialog, which) -> remindItems = new boolean[reminders.length])
                    .create().show();
        });

        startDatePicker = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
            startDate.set(year, month, dayOfMonth);
            startDateText.setText(format(startDate, 0));
            changeColor();
        }, startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));

        endDatePicker = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
            endDate.set(year, month, dayOfMonth);
            endDateText.setText(format(endDate, 0));
            changeColor();
        }, endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));

        startTimePicker = new TimePickerDialog(this, (view, hourOfDay, minute) -> {
            startDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
            startDate.set(Calendar.MINUTE, minute);
            startTimeText.setText(format(startDate, 1));
            changeColor();
        }, startDate.get(Calendar.HOUR_OF_DAY), startDate.get(Calendar.MINUTE), true);

        endTimePicker = new TimePickerDialog(this, (view, hourOfDay, minute) -> {
            endDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
            endDate.set(Calendar.MINUTE, minute);
            endTimeText.setText(format(endDate, 1));
            changeColor();
        }, endDate.get(Calendar.HOUR_OF_DAY), endDate.get(Calendar.MINUTE), true);

        startDateText.setOnClickListener(v -> startDatePicker.show());
        endDateText.setOnClickListener(v -> endDatePicker.show());
        startTimeText.setOnClickListener(v -> startTimePicker.show());
        endTimeText.setOnClickListener(v -> endTimePicker.show());
    }

    /**
     * 日期格式化
     *
     * @param calendar 日期
     * @param m        模式
     * @return 格式化的日期
     */
    public static String format(Calendar calendar, int m) {
        SimpleDateFormat sdf;
        if (m == 0) {
            sdf = new SimpleDateFormat("yyyy年MM月dd日 E");
            return sdf.format(calendar.getTime());
        } else if (m == 1) {
            sdf = new SimpleDateFormat("HH:mm");
            return sdf.format(calendar.getTime());
        } else { return "";}
    }

    /**
     * 提醒服务
     *
     * @param title 标题
     */
    private void reminder(String title) {
        if (remindItems != null) {
            if (remindItems[0]) {
                String url = getResources().getString(R.string.server_address) + "api/app/mail";

                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                JSONObject json = new JSONObject();
                startDate.getTimeInMillis();
                try {
                    json.put("time", startDate.getTimeInMillis());
                    json.put("token", user.getToken());
                    json.put("title", title);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
                    Log.i("response", response.toString());
                    try {
                        if (response.getInt("response") != 1) {// FIXME: 2021/1/11
                            Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Log.e("response", "邮件提醒出现错误"));
                requestQueue.add(request);
            }

            if (remindItems[1]) {
                Intent intent1 = new Intent(this, TimeService.class);
                intent1.removeExtra("title");
                intent1.putExtra("title", title);
                intent1.removeExtra("detail");
                EditText detail = findViewById(R.id.detail);
                intent1.putExtra("detail", detail.getText().toString());
                intent1.setFlags(PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
                Random r = new Random();
                PendingIntent pi = PendingIntent.getActivity(this, r.nextInt(100000), intent1, 0);
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, startDate.getTimeInMillis(), pi);
            }
        }
    }

    /**
     * 日期错误时显示红色
     */
    private void changeColor() {
        if (startDate.after(endDate)) {
            startDateText.setTextColor(Color.RED);
            startTimeText.setTextColor(Color.RED);
        } else {
            startDateText.setTextColor(Color.BLACK);
            startTimeText.setTextColor(Color.BLACK);
        }
    }

    /**
     * 在活动停止时调用
     */
    @Override
    protected void onStop() {
        startService(new Intent(this, SyncService.class));
        super.onStop();
    }
}