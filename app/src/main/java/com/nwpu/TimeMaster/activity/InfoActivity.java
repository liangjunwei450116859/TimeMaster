package com.nwpu.TimeMaster.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.data.User;
import org.json.JSONException;
import org.json.JSONObject;
import org.litepal.LitePal;

/**
 * 修改用户信息
 */
public class InfoActivity extends AppCompatActivity {
    /**
     * 用户
     */
    private User user;
    /**
     * 新Token
     */
    private String newToken;
    /**
     * 确认按钮
     */
    private Button acquire;
    /**
     * 计时器
     */
    private CountDownTimer timer;
    /**
     * 用户名
     */
    private EditText username;
    /**
     * 密码
     */
    private EditText email;
    /**
     * 验证码
     */
    private EditText code;

    /**
     * 活动启动时调用
     *
     * @param savedInstanceState 保存的实例状态
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        setTitle("修改信息");
        user = LitePal.find(User.class, getSharedPreferences("user", Context.MODE_PRIVATE).getInt("user_id", -1));
        username = findViewById(R.id.username);
        username.setText(user.getUsername());
        email = findViewById(R.id.email);
        email.setText(user.getEmail());
        code = findViewById(R.id.code);

        Button submit = findViewById(R.id.submit);
        submit.setOnClickListener(v -> {
            if (code.getText().length() == 0) {
                Toast.makeText(this, "验证码不能为空", Toast.LENGTH_SHORT).show();
                return;
            }
            set();
        });

        Button cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(v -> finish());

        acquire = findViewById(R.id.acquire);
        acquire.setOnClickListener(v -> {
            if (username.getText().length() == 0) {
                Toast.makeText(this, "用户名不能为空", Toast.LENGTH_SHORT).show();
                return;
            }
            if (email.getText().length() == 0) {
                Toast.makeText(this, "邮箱不能为空", Toast.LENGTH_SHORT).show();
                return;
            }
            captcha();
        });

        timer = new CountDownTimer(60 * 1000, 1000) {
            /**
             * Callback fired on regular interval.
             *
             * @param millisUntilFinished The amount of time until finished.
             */
            @Override
            public void onTick(long millisUntilFinished) {
                acquire.setClickable(false);
                acquire.setEnabled(false);
                acquire.setText("重新获取\n" + millisUntilFinished / 1000 + "s");
            }

            /**
             * Callback fired when the time is up.
             */
            @Override
            public void onFinish() {
                acquire.setText("重新获取");
                cancel(); // 取消倒计时
                acquire.setClickable(true);
                acquire.setEnabled(true);
            }
        };
    }

    /**
     * 发送请求
     */
    private void set() {
        String newUsername = username.getText().toString();
        String newEmail = email.getText().toString();

        String url = getResources().getString(R.string.server_address) + "api/app/setInfo";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        JSONObject json = new JSONObject();
        try {
            json.put("captcha", code.getText());
            json.put("token_old", user.getToken());
            json.put("token", newToken);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
            Log.i("response", response.toString());

            try {
                if (response.getInt("response") == 1) {
                    timer.cancel();
                    user.setUsername(newUsername);
                    user.setEmail(newEmail);
                    user.setToken(newToken);
                    user.update(user.getId());
                    Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(getApplication(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("response", "修改出现错误"));

        requestQueue.add(request);
    }

    /**
     * 获取验证码
     */
    private void captcha() {
        String url = getResources().getString(R.string.server_address) + "api/app/setInfo/captcha";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        JSONObject json = new JSONObject();
        try {
            json.put("username", username.getText());
            json.put("email", email.getText());
            json.put("token", user.getToken());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
            Log.i("response", response.toString());
            try {
                if (response.getInt("response") == 1) {
                    code.getText().clear();
                    username.setEnabled(false);
                    username.setEnabled(false);
                    email.setEnabled(false);
                    email.setEnabled(false);
                    newToken = response.getString("token");
                    timer.start();
                } else {
                    Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("response", "修改出现错误"));
        requestQueue.add(request);
    }
}