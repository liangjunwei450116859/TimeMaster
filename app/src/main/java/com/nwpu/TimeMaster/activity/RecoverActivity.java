package com.nwpu.TimeMaster.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.data.User;
import com.nwpu.TimeMaster.service.SyncService;
import org.json.JSONException;
import org.json.JSONObject;
import org.litepal.LitePal;

public class RecoverActivity extends AppCompatActivity {
    private EditText username;
    private EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover);

        Button submit = findViewById(R.id.submit);
        submit.setOnClickListener(v -> submit());

        Button cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(v -> finish());

        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
    }

    private void submit() {
        if (username.getText().length() == 0) {
            Toast.makeText(this, "用户名不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        if (email.getText().length() == 0) {
            Toast.makeText(this, "邮箱不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        String url = getResources().getString(R.string.server_address) + "api/app/recover";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        JSONObject json = new JSONObject();
        try {
            json.put("username", username.getText());
            json.put("email", email.getText());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
            Log.i("response", response.toString());

            try {
                if (response.getInt("response") == 1) {
                    Toast.makeText(this, "找回邮件已发送", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("response", "找回密码出现错误"));

        requestQueue.add(request);
    }
}