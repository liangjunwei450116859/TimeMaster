package com.nwpu.TimeMaster.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.data.User;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 用户注册
 */
public class RegisterActivity extends AppCompatActivity {
    /**
     * 用户名
     */
    private EditText username;
    /**
     * 邮箱
     */
    private EditText email;
    /**
     * 密码
     */
    private EditText password;
    /**
     * 验证码
     */
    private EditText code;
    /**
     * 获取验证码按钮
     */
    private Button acquire;
    /**
     * 计时器
     */
    private CountDownTimer timer;
    /**
     * Token
     */
    private String token;
    /**
     * 活动启动时调用
     *
     * @param savedInstanceState 保存的实例状态
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setTitle("注册");
        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        code = findViewById(R.id.code);

        Button register = findViewById(R.id.register);
        register.setOnClickListener(v -> {
            if (code.getText().length() == 0) {
                Toast.makeText(this, "验证码不能为空", Toast.LENGTH_SHORT).show();
                return;
            }
            register();
        });

        acquire = findViewById(R.id.acquire);
        acquire.setOnClickListener(v -> {
            if (username.getText().length() == 0) {
                Toast.makeText(this, "用户名不能为空", Toast.LENGTH_SHORT).show();
                return;
            }
            if (email.getText().length() == 0) {
                Toast.makeText(this, "邮箱不能为空", Toast.LENGTH_SHORT).show();
                return;
            }
            if (password.getText().length() == 0) {
                Toast.makeText(this, "密码不能为空", Toast.LENGTH_SHORT).show();
                return;
            }
            captcha();
        });

        timer = new CountDownTimer(60 * 1000, 1000) {
            /**
             * Callback fired on regular interval.
             *
             * @param millisUntilFinished The amount of time until finished.
             */
            @Override
            public void onTick(long millisUntilFinished) {
                acquire.setClickable(false);
                acquire.setEnabled(false);
                acquire.setText("重新获取\n" + millisUntilFinished / 1000 + "s");
            }

            /**
             * Callback fired when the time is up.
             */
            @Override
            public void onFinish() {
                acquire.setText("重新获取");
                cancel(); // 取消倒计时
                acquire.setClickable(true);
                acquire.setEnabled(true);
            }
        };
    }

    /**
     * 发送注册请求
     */
    private void register() {
        String url = getResources().getString(R.string.server_address) + "api/app/register";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        JSONObject json = new JSONObject();
        try {
            json.put("captcha", code.getText());
            json.put("token", token);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
            Log.i("response", response.toString());
            try {
                if (response.getInt("response") == 1) {
                    timer.cancel();
                    User user = new User(username.getText().toString(), password.getText().toString(), email.getText().toString(), token);
                    user.save();

                    SharedPreferences.Editor editor = getSharedPreferences("user", Context.MODE_PRIVATE).edit();
                    editor.putInt("user_id", user.getId());
                    editor.commit();

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    Toast.makeText(getApplicationContext(), "注册成功", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("response", "注册出现错误"));

        requestQueue.add(request);
    }

    /**
     * 获取验证码
     */
    private void captcha() {
        String url = getResources().getString(R.string.server_address) + "api/app/register/captcha";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        JSONObject json = new JSONObject();
        try {
            json.put("username", username.getText());
            json.put("email", email.getText());
            json.put("password", password.getText());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
            Log.i("response", response.toString());
            try {
                if (response.getInt("response") == 1) {
                    code.getText().clear();
                    username.setEnabled(false);
                    username.setEnabled(false);
                    email.setEnabled(false);
                    email.setEnabled(false);
                    password.setEnabled(false);
                    password.setEnabled(false);

                    timer.start();
                    token = response.getString("token");
                } else {
                    Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("response", "注册出现错误"));

        requestQueue.add(request);
    }

}