package com.nwpu.TimeMaster.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.data.User;
import com.nwpu.TimeMaster.service.SyncService;
import org.json.JSONException;
import org.json.JSONObject;
import org.litepal.LitePal;

/**
 * 用户登录
 */
public class LoginActivity extends AppCompatActivity {
    /**
     * 用户名
     */
    private EditText username;
    /**
     * 密码
     */
    private EditText password;
    /**
     * 共享变量，用于存储用户ID
     */
    private SharedPreferences share;
    /**
     * 活动启动时调用
     *
     * @param savedInstanceState 保存的实例状态
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setTitle("登录");
        share = getSharedPreferences("user", Context.MODE_PRIVATE);

        if (share.contains("user_id")) {
            startService(new Intent(this, SyncService.class));
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        Button login = findViewById(R.id.login);
        login.setOnClickListener(v -> login());

        Button register = findViewById(R.id.register);
        register.setOnClickListener(v -> startActivity(new Intent(this, RegisterActivity.class)));

        TextView recover=findViewById(R.id.recover);
        recover.setOnClickListener(v -> startActivity(new Intent(this,RecoverActivity.class)));
    }

    /**
     * 发送登录请求
     */
    private void login() {
        String url = getResources().getString(R.string.server_address) + "api/app/login";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        JSONObject json = new JSONObject();
        try {
            json.put("username", username.getText());
            json.put("password", password.getText());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
            Log.i("response", response.toString());

            try {
                if (response.getInt("response") == 1) {
                    startService(new Intent(this, SyncService.class));
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    User user = LitePal.where("username = ?", username.getText().toString()).findFirst(User.class);
                    if (user == null) {
                        user = new User(username.getText().toString(), password.getText().toString());
                        user.save();
                    }
                    user.setEmail(response.getString("email"));
                    user.setToken(response.getString("token"));
                    user.update(user.getId());

                    SharedPreferences.Editor editor = share.edit();
                    editor.putInt("user_id", user.getId());
                    editor.commit();

                    Toast.makeText(getApplicationContext(), "登录成功", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("response", "登录出现错误"));

        requestQueue.add(request);
    }
}