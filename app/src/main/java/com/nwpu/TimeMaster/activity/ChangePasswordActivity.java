package com.nwpu.TimeMaster.activity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.data.User;
import org.json.JSONException;
import org.json.JSONObject;
import org.litepal.LitePal;

/**
 * 修改密码
 */
public class ChangePasswordActivity extends AppCompatActivity {
    /**
     * 用户
     */
    private User user;
    /**
     * 输入框（旧密码，新密码，确认密码）
     */
    private EditText oldPasswordText, newPasswordText, confirmPasswordText;

    /**
     * 活动启动时调用
     *
     * @param savedInstanceState 保存的实例状态
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        setTitle("修改密码");
        user = LitePal.find(User.class, getSharedPreferences("user", Context.MODE_PRIVATE).getInt("user_id", -1));
        oldPasswordText = findViewById(R.id.old_password);
        newPasswordText = findViewById(R.id.new_password);
        confirmPasswordText = findViewById(R.id.confirm_password);

        Button submit = findViewById(R.id.submit);
        submit.setOnClickListener(v -> {
            if (oldPasswordText.getText().length() == 0) {
                Toast.makeText(this, "旧密码不能为空", Toast.LENGTH_SHORT).show();
                return;
            }
            if (newPasswordText.getText().length() == 0) {
                Toast.makeText(this, "新密码不能为空", Toast.LENGTH_SHORT).show();
                return;
            }
            if (confirmPasswordText.getText().length() == 0) {
                Toast.makeText(this, "确认密码不能为空", Toast.LENGTH_SHORT).show();
                return;
            }
            set();
        });

        Button cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(v -> finish());
    }

    /**
     * 发送请求
     */
    private void set() {
        String oldPassword = oldPasswordText.getText().toString();
        String newPassword = newPasswordText.getText().toString();
        String confirmPassword = confirmPasswordText.getText().toString();

        if (!newPassword.equals(confirmPassword)) {
            Toast.makeText(this, "两次密码不一致", Toast.LENGTH_SHORT).show();
            newPasswordText.getText().clear();
            confirmPasswordText.getText().clear();
            return;
        }
        if (!oldPassword.equals(user.getPassword())) {
            Toast.makeText(this, "旧密码错误", Toast.LENGTH_SHORT).show();
            oldPasswordText.getText().clear();
            return;
        }

        String url = getResources().getString(R.string.server_address) + "api/app/setPassword";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        JSONObject json = new JSONObject();
        try {
            json.put("password", newPassword);
            json.put("token", user.getToken());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
            Log.i("response", response.toString());

            try {
                if (response.getInt("response") == 1) {
                    user.setPassword(newPassword);
                    user.setToken(response.getString("token"));
                    user.update(user.getId());
                    Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("response", "修改出现错误"));

        requestQueue.add(request);
    }
}