package com.nwpu.TimeMaster.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.nwpu.TimeMaster.R;
import com.google.android.material.navigation.NavigationView;
import com.nwpu.TimeMaster.data.User;
import org.litepal.LitePal;

/**
 * 主活动
 */
public class MainActivity extends AppCompatActivity {
    /**
     * 应用栏配置
     */
    private AppBarConfiguration mAppBarConfiguration;

    /**
     * 活动启动时调用
     *
     * @param savedInstanceState 保存的实例状态
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainInit();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow, R.id.nav_setting, R.id.nav_about)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    /**
     * 向上导航
     *
     * @return 导航是否成功完成
     */
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
               || super.onSupportNavigateUp();
    }

    /**
     * 初始化
     */
    public void MainInit() {
        NavigationView view = findViewById(R.id.nav_view);
        View headerView = view.getHeaderView(0);
        User user = LitePal.find(User.class, getSharedPreferences("user", Context.MODE_PRIVATE).getInt("user_id", -1));

        TextView textView = headerView.findViewById(R.id.nav_header_username);
        textView.setText(user.getUsername());
        textView = headerView.findViewById(R.id.nav_header_email);
        textView.setText(user.getEmail());
    }
}