package com.nwpu.TimeMaster.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.nwpu.TimeMaster.R;

/**
 * 同步服务
 */
public class TimeService extends AppCompatActivity {
    /**
     * 活动启动时调用
     *
     * @param savedInstanceState 保存的实例状态
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        intent.removeExtra("title");
        String detail = intent.getStringExtra("detail");
        intent.removeExtra("detail");
        Notification notification;
        Notification.Builder builder;
        Context mContext = TimeService.this;
        NotificationManager mNManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNManager.deleteNotificationChannel("5996773");
            NotificationChannel channel = new NotificationChannel("5996773", "安卓10a", NotificationManager.IMPORTANCE_HIGH);
            channel.enableLights(true);//是否在桌面icon右上角展示小红点
            channel.setLightColor(Color.GREEN);//小红点颜色
            channel.setShowBadge(false); //是否在久按桌面图标时显示此渠道的通知
            channel.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.biaobiao), Notification.AUDIO_ATTRIBUTES_DEFAULT);
            mNManager.createNotificationChannel(channel);
            builder = new Notification.Builder(mContext, "5996773");
        } else {
            builder = new Notification.Builder(mContext);
        }
        //设置标题
        builder.setContentTitle(title);
        //设置内容
        builder.setContentText(detail);
        //设置状态栏显示的图标，建议图标颜色透明
        builder.setSmallIcon(R.mipmap.ic_launcher);
        // 设置通知灯光（LIGHTS）、铃声（SOUND）、震动（VIBRATE）、（ALL 表示都设置）
        builder.setDefaults(Notification.DEFAULT_ALL);
        //灯光三个参数，颜色（argb）、亮时间（毫秒）、暗时间（毫秒）,灯光与设备有关
        builder.setLights(Color.RED, 200, 200);
        // 震动，传入一个 long 型数组，表示 停、震、停、震 ... （毫秒）
        builder.setVibrate(new long[]{0, 200, 200, 200, 200, 200});
        // 通知栏点击后自动消失
        builder.setAutoCancel(true);
        // 简单通知栏设置 Intent
        // builder.setContentIntent(pendingIntent);
        builder.setPriority(Notification.PRIORITY_HIGH);
        //设置下拉之后显示的图片
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        notification = builder.build();
        mNManager.notify(1, notification);
        finish();
    }
}