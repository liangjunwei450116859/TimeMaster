package com.nwpu.TimeMaster.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.data.ToDo;
import com.nwpu.TimeMaster.data.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.litepal.LitePal;

import java.util.Date;
import java.util.List;

/**
 * 同步服务
 */
public class SyncService extends Service {
    /**
     * 用户
     */
    private User user;
    /**
     * 待办事项列表
     */
    private List<ToDo> toDoList;
    /**
     * 请求队列
     */
    private RequestQueue requestQueue;
    /**
     * Json数据
     */
    private JSONObject json;

    /**
     * 构造器
     */
    public SyncService() {
    }

    /**
     * 客户端显式启动服务时，由系统调用的方法
     *
     * @param intent  提供给Context.startService的意图
     * @param flags   启动请求的附加数据
     * @param startId 一个唯一的整数，代表这个启动请求
     * @return 服务的当前启动状态使用什么语义
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("SyncService", "同步服务开启");
        user = LitePal.find(User.class, getSharedPreferences("user", Context.MODE_PRIVATE).getInt("user_id", -1));
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        try {
            receiveData();
            sendData();
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "同步失败", Toast.LENGTH_SHORT).show();
        }
        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 发送数据
     *
     * @throws JSONException json解析异常
     */
    private void sendData() throws JSONException {
        String url = getResources().getString(R.string.server_address) + "api/app/sync/up";
        json = new JSONObject();
        toDoList = LitePal.where("user_id = ? and status < ?", String.valueOf(user.getId()), "9").find(ToDo.class);

        json.put("token", user.getToken());
        json.put("todo_list", createJsonArray(toDoList));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
            Log.i("response", response.toString());

            try {
                JSONArray array = new JSONArray(response.getString("response"));
                Date anchor = new Date(response.getLong("anchor"));
                if (array.length() == 1 && array.getInt(0) == 0) {
//                    Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    for (int i = 0; i < array.length(); i++) {
                        ToDo toDo = toDoList.get(i);
                        switch (array.getInt(i)) {
                            case 8:
                                toDo.delete();
                                break;
                            case 9:
                                toDo.setStatus(9);
                                toDo.setAnchor(anchor);
                                toDo.update(toDo.getId());
                            default:
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("response", "同步出现错误"));

        requestQueue.add(request);
    }

    /**
     * 接受数据
     *
     * @throws JSONException json解析异常
     */
    private void receiveData() throws JSONException {
        String url = getResources().getString(R.string.server_address) + "api/app/sync/down";
        json = new JSONObject();
        long maxAnchor = LitePal.where("user_id = ?", String.valueOf(user.getId())).max(ToDo.class, "anchor", Long.class);
        json.put("token", user.getToken());
        json.put("max_anchor", maxAnchor);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
            Log.i("response", response.toString());

            try {
                if (response.getInt("response") != 1) {
//                    Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    JSONArray array = new JSONArray(response.getString("todo_list")), aToDo;
                    for (int i = 0; i < array.length(); i++) {
                        aToDo = array.getJSONArray(i);
                        Date creationDate = new Date(aToDo.getLong(0));
                        Date startDate = new Date(aToDo.getLong(1));
                        Date endDate = new Date(aToDo.getLong(2));
                        String title = aToDo.getString(3);
                        String detail = aToDo.getString(4);
                        int priority = aToDo.getInt(5);
                        int tag = aToDo.getInt(6);
                        Date anchor = new Date(aToDo.getLong(7));
                        new ToDo(user, creationDate, startDate, endDate, title, detail, priority, tag, 9, anchor).save();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("response", "同步出现错误"));

        requestQueue.add(request);
    }

    /**
     * 创建Json数组
     *
     * @param list 待办事项列表
     * @return Json数组
     */
    private JSONArray createJsonArray(List<ToDo> list) {
        JSONArray array = new JSONArray();
        JSONArray todoArray;
        for (ToDo toDo : list) {
            todoArray = new JSONArray();
            todoArray.put(toDo.getStatus())
                    .put(toDo.getCreationDate().getTime())
                    .put(toDo.getStartDate().getTime())
                    .put(toDo.getEndDate().getTime())
                    .put(toDo.getTitle())
                    .put(toDo.getDetail())
                    .put(toDo.getPriority())
                    .put(toDo.getTag())
                    .put(toDo.getAnchor().getTime());
            array.put(todoArray);
        }
        return array;
    }

    /**
     * 服务销毁时调用
     */
    @Override
    public void onDestroy() {
        Log.i("SyncService", "同步服务关闭");
        super.onDestroy();
    }

    /**
     * 服务被绑定时调用
     *
     * @param intent 启动意图
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
