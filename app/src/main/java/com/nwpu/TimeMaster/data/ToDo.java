package com.nwpu.TimeMaster.data;

import org.litepal.annotation.Column;
import org.litepal.crud.LitePalSupport;

import java.util.Date;

/**
 * 待办事项实体类
 */
public class ToDo extends LitePalSupport {
    /**
     * ID
     */
    private int id;
    @Column(nullable = false)
    /**
     * 用户
     */
    private User user;
    /**
     * 创建日期
     */
    @Column(nullable = false)
    private Date creationDate;
    /**
     * 开始日期
     */
    @Column(nullable = false)
    private Date startDate;
    /**
     * 结束日期
     */
    private Date endDate;
    /**
     * 标题
     */
    @Column(nullable = false)
    private String title;
    /**
     * 内容
     */
    private String detail;
    /**
     * 优先级
     */
    @Column(nullable = false, defaultValue = "3")
    private Integer priority;
    /**
     * 标签
     */
    @Column(nullable = false, defaultValue = "4")
    private Integer tag;
    /**
     * 状态
     * <p>
     * 0  本地新增
     * -1 标记删除
     * 1  本地更新
     * 9  已同步
     */
    private int status;
    /**
     * 同步锚点
     */
    private Date anchor;

    /**
     * 构造器
     */
    public ToDo() {
    }

    /**
     * 构造器
     */
    public ToDo(User user, Date startDate, Date endDate, String title, String detail, Integer priority, Integer tag, int status, Date anchor) {
        this.user = user;
        this.creationDate = new Date();
        this.startDate = startDate;
        this.endDate = endDate;
        this.title = title;
        this.detail = detail;
        this.priority = priority;
        this.tag = tag;
        this.status = status;
        this.anchor = anchor;
    }

    /**
     * 构造器
     */
    public ToDo(User user, Date creationDate, Date startDate, Date endDate, String title, String detail, Integer priority, Integer tag, int status, Date anchor) {
        this.user = user;
        this.creationDate = creationDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.title = title;
        this.detail = detail;
        this.priority = priority;
        this.tag = tag;
        this.status = status;
        this.anchor = anchor;
    }

    public int getId() { return id; }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public User getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getAnchor() {
        return anchor;
    }

    public void setAnchor(Date anchor) {
        this.anchor = anchor;
    }
}
