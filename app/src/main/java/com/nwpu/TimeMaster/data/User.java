package com.nwpu.TimeMaster.data;

import org.litepal.annotation.Column;
import org.litepal.crud.LitePalSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户实体类
 */
public class User extends LitePalSupport {
    /**
     * ID
     */
    private int id;
    /**
     * 用户名
     */
    @Column(unique = true, index = true)
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 邮箱
     */
    private String email;
    /**
     * Token
     */
    private String token;
    /**
     * 待办事项列表
     */
    private List<ToDo> ToDoList = new ArrayList<>();

    /**
     * 构造器
     */
    public User() {
    }

    /**
     * 构造器
     */
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * 构造器
     */
    public User(String username, String password, String email, String token) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.token = token;
    }

    /**
     * 重写equals方法
     *
     * @param o 要比较的对象
     * @return 是否相等
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        User user = (User) o;

        return username.equals(user.username);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<ToDo> getToDoList() {
        return ToDoList;
    }

    public void setToDoList(List<ToDo> toDoList) {
        ToDoList = toDoList;
    }
}
