package com.nwpu.TimeMaster.fragment;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.nwpu.TimeMaster.data.ToDo;
import org.litepal.LitePal;

import java.util.List;

/**
 *“首页”模块的存储数据类
 */
public class HomeViewModel extends AndroidViewModel {
    /**
     *数据实体类，存储当前用户的所有待办事项
     */
    private final MutableLiveData<List<ToDo>> todos;

    /**
     * 搜索获取当前用户的所有待办事项
     *
     * @param application 存储系统信息
     */
    public HomeViewModel(@NonNull Application application) {
        super(application);
        List<ToDo> toDoList = LitePal.where("user_id = ? and status >= 0", String.valueOf(getApplication().getSharedPreferences("user", Context.MODE_PRIVATE).getInt("user_id", -1))).find(ToDo.class);
        todos = new MutableLiveData<>(toDoList);
    }

    /**
     * 获取todos
     *
     * @return todos
     */
    public LiveData<List<ToDo>> getToDos() {
        return todos;
    }
}