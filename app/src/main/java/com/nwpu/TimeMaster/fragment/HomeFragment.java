package com.nwpu.TimeMaster.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.alamkanak.weekview.WeekView;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.activity.AddToDoActivity;
import com.nwpu.TimeMaster.adapter.WeekViewAdapter;
import com.nwpu.TimeMaster.data.ToDo;

import java.util.Calendar;

/**
 * 主活动中“首页”模块
 */
public class HomeFragment extends Fragment {

    private WeekView weekView;

    /**
     * fragment启动时调用进行初始化
     *
     * @param savedInstanceState 保存的实例状态
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //让Fragment中的菜单项在toolbar显示出来
        setHasOptionsMenu(true);
    }

    /**
     *创建该fragment对应的视图
     *
     * @param inflater 将XML转换为视图对象
     * @param container 父容器的引用
     * @param savedInstanceState 保存的状态
     * @return fragment创建的view
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        HomeViewModel homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        ImageButton button = root.findViewById(R.id.add_button);
        button.setOnClickListener(view -> startActivity(new Intent(getActivity(), AddToDoActivity.class)));

        weekView = root.findViewById(R.id.weekView);
        WeekView.SimpleAdapter<ToDo> adapter = new WeekViewAdapter();
        weekView.setAdapter(adapter);
        weekView.setShowWeekNumber(true);
        weekView.setShowCompleteDay(false);
        weekView.setFutureWeekendBackgroundColor(0xFFFAEBD7);
        weekView.setShowNowLine(true);
        homeViewModel.getToDos().observe(this, adapter::submitList);

        return root;
    }

    /**
     *初始化菜单
     *
     * @param menu 显示的menu的xml实例
     * @param inflater 将XML转换为视图对象
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main_activity, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * menu点击事件
     *
     * @param item 点击的按钮
     * @return 点击事件是否继续向下传递
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_calendar:
                Calendar calendar = weekView.getFirstVisibleDate();
                DatePickerDialog datePicker = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {
                    calendar.set(year, month, dayOfMonth);
                    weekView.scrollToDate(calendar);
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePicker.show();
                return true;
            case R.id.action_today:
                weekView.scrollToDateTime(Calendar.getInstance());
                return true;
            case R.id.action_oneday:
                weekView.setNumberOfVisibleDays(1);
                return true;
            case R.id.action_threedays:
                weekView.setNumberOfVisibleDays(3);
                return true;
            case R.id.action_sevendays:
                weekView.setNumberOfVisibleDays(7);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}