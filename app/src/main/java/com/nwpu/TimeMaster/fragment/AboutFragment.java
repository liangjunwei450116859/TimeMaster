package com.nwpu.TimeMaster.fragment;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nwpu.TimeMaster.R;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * 主活动的“关于”模块
 */
public class AboutFragment extends Fragment {

    /**
     * 服务端url
     */
    private String url;

    /**
     * 创建该fragment对应的视图
     *
     * @param inflater           将XML转换为视图对象
     * @param container          父容器的引用
     * @param savedInstanceState 保存的状态
     * @return fragment创建的view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        TextView update = view.findViewById(R.id.update);
        update.setOnClickListener(v -> set());
        return view;
    }

    /**
     *
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * 发送请求报文
     */
    public void set() {
        int versionCode;
        url = getResources().getString(R.string.server_address) + "api/app/update";
        /*
         * 请求队列
         */
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JSONObject json = new JSONObject();
        try {
            versionCode = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionCode;
            json.put("version", versionCode);
        } catch (PackageManager.NameNotFoundException | JSONException e) {
            Toast.makeText(getContext(), "更新失败", Toast.LENGTH_SHORT).show();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, json, response -> {
            Log.i("response", response.toString());
            try {
                if (response.getInt("response") != 1) {
                    Toast.makeText(getContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    if (response.getInt("update") == 1) {
                        update();
                    } else {
                        Toast.makeText(getContext(), "没有找到新版本", Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("response", "更新出现错误"));

        requestQueue.add(request);
    }

    /**
     * 下载新版软件
     */
    private void update() {
        url = getResources().getString(R.string.server_address) + "api/app/apk";
        new AlertDialog.Builder(getContext()).setTitle("新版本更新可用，是否下载？")
                .setPositiveButton("确定", (dialog, which) -> {
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                    DownloadManager downloadManager = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "TimeMaster_update.apk");
                    downloadManager.enqueue(request);
                    Toast.makeText(getContext(), "下载完成", Toast.LENGTH_SHORT).show();
                })
                .setNegativeButton("取消", null)
                .create().show();
    }
}