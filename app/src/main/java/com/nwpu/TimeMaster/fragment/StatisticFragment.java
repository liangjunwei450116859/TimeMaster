package com.nwpu.TimeMaster.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.data.ToDo;
import com.nwpu.TimeMaster.data.User;
import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

public class StatisticFragment extends Fragment {

    User user;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = View.inflate(getActivity(), R.layout.fragment_statistic, null);
        user = LitePal.find(User.class, getActivity().getSharedPreferences("user", Context.MODE_PRIVATE).getInt("user_id", -1));
        initTagChart(root);
        initPriorityChart(root);
        return root;
    }

    private void initPriorityChart(View view) {
        BarChart priorityChart = view.findViewById(R.id.priority_chart);
        String[] priorities = getResources().getStringArray(R.array.priority);
        List<BarEntry> entryList = new ArrayList<>();
        for (int i = 0; i < priorities.length; i++) {
            entryList.add(new BarEntry(i, LitePal.where("user_id = ? and priority = ? and status >= 0", String.valueOf(user.getId()), String.valueOf(i)).count(ToDo.class)));
        }
        initBarChart(priorityChart, priorities, entryList, 0xFFFFD700);
    }

    private void initTagChart(View view) {
        BarChart tagChart = view.findViewById(R.id.tag_chart);
        String[] tags = getResources().getStringArray(R.array.tag);
        List<BarEntry> entryList = new ArrayList<>();
        for (int i = 0; i < tags.length; i++) {
            entryList.add(new BarEntry(i, LitePal.where("user_id = ? and tag = ? and status >= 0", String.valueOf(user.getId()), String.valueOf(i)).count(ToDo.class)));
        }
        initBarChart(tagChart, tags, entryList, 0xFF1E90FF);
    }

    private void initBarChart(BarChart barChart, String[] items, List<BarEntry> entryList, int color) {
        BarDataSet dataSet = new BarDataSet(entryList, null);
        dataSet.setColor(color);
        dataSet.setBarShadowColor(0xFFF5F5F5);
        dataSet.setValueTextSize(20f);
        dataSet.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.valueOf((int) value);
            }
        });
        barChart.setTouchEnabled(false);
        barChart.animateY(500);
        barChart.getAxisRight().setEnabled(false);
        barChart.getAxisLeft().setEnabled(false);
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
        barChart.getXAxis().setDrawAxisLine(false);
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getXAxis().setLabelCount(items.length);
        barChart.getXAxis().setTextSize(20f);
        barChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return items[(int) value];
            }
        });
        barChart.getLegend().setEnabled(false);
        barChart.getDescription().setEnabled(false);
        barChart.setNoDataText("无数据");
        barChart.setNoDataTextColor(Color.BLACK);
        barChart.setDrawBarShadow(true);
        barChart.setData(new BarData(dataSet));
    }
}