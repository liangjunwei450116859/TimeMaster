package com.nwpu.TimeMaster.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.*;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.data.ToDo;
import com.nwpu.TimeMaster.data.User;
import org.litepal.LitePal;

import java.text.SimpleDateFormat;
import java.util.*;

public class ListViewFragment extends Fragment {
    /**
     * 当前用户
     */
    private User user;
    /**
     * 当前用户的所有待办事项
     */
    private List<ToDo> toDoList;
    /**
     * 待办事项显示样式
     */
    private ListView lv;
    /**
     * 待办事项显示的数据
     */
    private String[] from;
    /**
     * 显示样式的部件
     */
    private int[] to;

    /**
     * 创建该fragment对应的视图
     *
     * @param inflater           将XML转换为视图对象
     * @param container          父容器的引用
     * @param savedInstanceState 保存的状态
     * @return fragment创建的view
     */
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = View.inflate(getActivity(), R.layout.fragment_test_to_do_list, null);

        user = LitePal.find(User.class, getActivity().getSharedPreferences("user", Context.MODE_PRIVATE).getInt("user_id", -1));
        toDoList = LitePal.where("user_id=? and status >= 0", String.valueOf(user.getId())).order("startdate desc").find(ToDo.class);
        lv = root.findViewById(R.id.lv);
        from = new String[]{"title", "start_time", "priority"};
        to = new int[]{R.id.tv_1, R.id.tv_2, R.id.priority};
        if (lv != null) {
            lv.setAdapter(new SimpleAdapter(getContext(), getListOfMap(toDoList), R.layout.sample_my_view, from, to));
        }

        return root;
    }

    /**
     * 设置显示的数据
     *
     * @param toDoList 当前用户的所有待办事项
     * @return 数据对应表
     */
    public List<Map<String, String>> getListOfMap(List<ToDo> toDoList) {
        List<Map<String, String>> resList = new ArrayList<>();
        Resources res = getResources();
        String[] priorityShorts = res.getStringArray(R.array.priorityShort);
        for (ToDo toDo : toDoList) {
            Map<String, String> data = new HashMap<>();
            data.put("title", toDo.getTitle());
            data.put("start_time", format(toDo.getStartDate(), 2));
            data.put("priority", priorityShorts[toDo.getPriority()]);
            resList.add(data);
        }
        return resList;
    }

    /**
     * 该fragment的初始化
     *
     * @param savedInstanceState 保存的状态
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * 初始化菜单
     *
     * @param menu     显示的menu的xml实例
     * @param inflater 将XML转换为视图对象
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.todo_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * menu点击事件
     *
     * @param item 点击的按钮
     * @return 点击事件是否继续向下传递
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_prioritySort:
                toDoList = LitePal.where("user_id=? and status >= 0", String.valueOf(user.getId())).order("priority, startdate desc").find(ToDo.class);
                if (lv != null) {
                    lv.setAdapter(new SimpleAdapter(getContext(), getListOfMap(toDoList), R.layout.sample_my_view, from, to));
                }
                return true;
            case R.id.action_startdateSort:
                toDoList = LitePal.where("user_id=? and status >= 0", String.valueOf(user.getId())).order("startdate desc").find(ToDo.class);
                if (lv != null) {
                    lv.setAdapter(new SimpleAdapter(getContext(), getListOfMap(toDoList), R.layout.sample_my_view, from, to));
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * date转换成string
     *
     * @param date 时期
     * @param m    选项
     * @return 时期字符串
     */
    public static String format(Date date, int m) {
        SimpleDateFormat sdf;
        if (m == 0) {
            sdf = new SimpleDateFormat("yyyy年MM月dd日 E");
            return sdf.format(date);
        } else if (m == 1) {
            sdf = new SimpleDateFormat("HH:mm");
            return sdf.format(date);
        } else if (m == 2) {
            sdf = new SimpleDateFormat("yyyy年MM月dd日 E HH:mm");
            return sdf.format(date);
        } else { return "";}
    }
}