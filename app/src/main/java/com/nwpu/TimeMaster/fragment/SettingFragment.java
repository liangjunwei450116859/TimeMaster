package com.nwpu.TimeMaster.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import com.nwpu.TimeMaster.R;
import com.nwpu.TimeMaster.activity.ChangePasswordActivity;
import com.nwpu.TimeMaster.activity.InfoActivity;
import com.nwpu.TimeMaster.activity.LoginActivity;
import com.nwpu.TimeMaster.data.User;
import org.litepal.LitePal;

import java.util.Objects;

/**
 * 主活动中的“设置”模块
 */
public class SettingFragment extends Fragment {
    /**
     * 当前登录的用户
     */
    private User user;
    /**
     * 退出view，修改view， 密码view
     */
    private TextView logout, info, password;

    /**
     * 该fragment的初始化
     *
     * @param savedInstanceState 保存的状态
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * 创建该fragment对应的视图
     *
     * @param inflater           将XML转换为视图对象
     * @param container          父容器的引用
     * @param savedInstanceState 保存的状态
     * @return fragment创建的view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        SharedPreferences share = Objects.requireNonNull(getActivity()).getSharedPreferences("user", Context.MODE_PRIVATE);

        user = LitePal.find(User.class, share.getInt("user_id", -1), true);

        logout = view.findViewById(R.id.logout);
        logout.setOnClickListener(v -> {
            SharedPreferences.Editor editor = share.edit();

            editor.remove("user_id");
            editor.commit();

            Intent intent = new Intent(getContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });

        info = view.findViewById(R.id.info);
        info.setOnClickListener(v -> startActivity(new Intent(getContext(), InfoActivity.class)));

        password = view.findViewById(R.id.password);
        password.setOnClickListener(v -> startActivity(new Intent(getContext(), ChangePasswordActivity.class)));
        return view;
    }

}